package com.home_test.aroundsydney.common;

public class Constant {


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;
    public static final String LOCATION_PERMISSION_GRANTED_EVENT = "LOCATION_PERMISSION_GRANTED_EVENT";

    public static final long LOCATION_REFRESH_TIME = 60000; //milliseconds
    public static final float LOCATION_REFRESH_DISTANCE = 100; //meters

    public static final String LOCATION_UPDATE_EVENT = "LOCATION_UPDATE_EVENT";
    public static final String LOCATIONS_DATA_UPDATE_EVENT = "LOCATIONS_DATA_UPDATE_EVENT";
    public static final String LOCATION_DETAILS_EXTRA_KEY = "LOCATION_DETAILS_EXTRA_KEY";


}
