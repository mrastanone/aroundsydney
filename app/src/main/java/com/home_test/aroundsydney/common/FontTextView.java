package com.home_test.aroundsydney.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.home_test.aroundsydney.R;

/**
 * Created by masteralex on 7/6/17.
 */

public class FontTextView extends AppCompatTextView {

    public FontTextView(Context context) {
        this(context, null);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isInEditMode())
            return;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FontText);
        if (ta != null) {
            String fontAsset = ta.getString(R.styleable.FontText_typefaceAsset);
            if (fontAsset!=null && !fontAsset.isEmpty()) {
                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), fontAsset);
                setTypeface(tf);
            }
        }
    }
}
