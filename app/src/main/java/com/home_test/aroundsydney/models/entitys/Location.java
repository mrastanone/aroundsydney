package com.home_test.aroundsydney.models.entitys;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class Location implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @SerializedName("lat")
    public double latitude;

    @SerializedName("lng")
    public double longitude;

    @SerializedName("name")
    public String name;

    public String note;

    public float distance = 0;

    public boolean isItemFromRemote;

}
