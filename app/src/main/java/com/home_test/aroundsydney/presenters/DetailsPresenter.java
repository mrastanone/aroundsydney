package com.home_test.aroundsydney.presenters;

import com.arellomobile.mvp.MvpPresenter;
import com.home_test.aroundsydney.common.AroundSydneyApplication;
import com.home_test.aroundsydney.models.AppLocationModel;
import com.home_test.aroundsydney.models.entitys.Location;
import com.home_test.aroundsydney.views.DetailsView;

import javax.inject.Inject;

public class DetailsPresenter extends MvpPresenter<DetailsView> {

    @Inject
    public AppLocationModel locationModel;

    public DetailsPresenter() {
        AroundSydneyApplication.getAppComponent().inject(this);
    }


    public float getDistanceForLocation(Location location) {
        return locationModel.calculateDistanceBetweenMyLocations(location);
    }

    public void updateLocationData(Location location) {
        locationModel.createOrUpdateLocation(location);
    }

}
