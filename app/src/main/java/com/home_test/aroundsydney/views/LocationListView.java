package com.home_test.aroundsydney.views;

import com.arellomobile.mvp.MvpView;
import com.home_test.aroundsydney.models.entitys.Location;

import java.util.List;

public interface LocationListView extends MvpView {


    void showListData(List<Location> locations);

    void clearList();
}
