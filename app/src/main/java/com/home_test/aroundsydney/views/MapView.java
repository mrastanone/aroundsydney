package com.home_test.aroundsydney.views;

import com.arellomobile.mvp.MvpView;
import com.home_test.aroundsydney.models.entitys.Location;

import java.util.List;

public interface MapView extends MvpView {

    void showPins(List<Location> locations);

    void addPin(Location location);

}
