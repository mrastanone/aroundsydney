package com.home_test.aroundsydney.models;

import com.home_test.aroundsydney.models.entitys.Location;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LocationModelTest {

    @Mock
    LocationModel locationModel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void loadLocations_shouldReturnLocationList() {
        final List<Location> locationsBuffer = new ArrayList<>();
        doReturn(Observable.just(locationsBuffer)).when(locationModel).getLocations();
        Observable<List<Location>> observer = locationModel.getLocations();
//        verify(observer).doOnNext()


//        locationModel.getLocations().subscribe(new Consumer<List<Location>>() {
//            @Override
//            public void accept(List<Location> locations) {
//                locationsBuffer.addAll(locations);
//            }
//        }, new Consumer<Throwable>() {
//            @Override
//            public void accept(Throwable throwable) {
//
//            }
//        }, new Action() {
//            @Override
//            public void run() {
//                assertTrue(!locationsBuffer.isEmpty());
//            }
//        });
    }

}
