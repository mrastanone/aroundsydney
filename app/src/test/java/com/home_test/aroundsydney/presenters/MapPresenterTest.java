package com.home_test.aroundsydney.presenters;

import com.google.android.gms.maps.model.LatLng;
import com.home_test.aroundsydney.common.di.AppComponent;
import com.home_test.aroundsydney.models.LocationModel;
import com.home_test.aroundsydney.models.entitys.Location;
import com.home_test.aroundsydney.models.repositories.local.LocalDBRepository;
import com.home_test.aroundsydney.models.repositories.remote.AmazonRepository;
import com.home_test.aroundsydney.views.MapView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.plugins.RxJavaPlugins;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MapPresenterTest {

    @Mock
    MapPresenter presenter;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        RxJavaPlugins.reset();
    }


    @Test
    public void test_requestLocations() {
//        final AppComponent mockedAppComponent = mock(AppComponent.class);
//
//        final AmazonRepository mockedAmazonRepository = mock(AmazonRepository.class);
//        final LocalDBRepository mockedLocalDBRepository = mock(LocalDBRepository.class);
//
//        final LocationModel locationModel = new LocationModel(mockedAppComponent, mockedAmazonRepository, mockedLocalDBRepository);
//
//        locationModel.amazonRepository = mockedAmazonRepository;
//        locationModel.localDBRepository = mockedLocalDBRepository;

        final LocationModel mockedLocationModel = mock(LocationModel.class);
        final MapView mockedMapView = mock(MapView.class);

        presenter.locationModel = mockedLocationModel;
        presenter.requestLocations();

        verify(mockedLocationModel).getLocations();
//        verify(mockedMapView).showPins(any(List.class));
    }

    @Test
    public void test_addLocation() {
        final LocationModel mockedLocationModel = mock(LocationModel.class);
        presenter.locationModel = mockedLocationModel;

        final MapView mockedMapView = mock(MapView.class);
        presenter.attachView(mockedMapView);
        presenter.addCustomLocation(new LatLng(0, 0), "Fake name");

        ArgumentCaptor<Location> argument = ArgumentCaptor.forClass(Location.class);
        verify(presenter).addCustomLocation(new LatLng(0, 0), "Fake name");

        assertEquals("Fake name", argument.getValue().name);
        assertEquals(0, argument.getValue().latitude, 0.1);
        assertEquals(0, argument.getValue().longitude, 0.1);


        verify(mockedLocationModel).createOrUpdateLocation(argument.getValue());
        verify(mockedMapView).addPin(any(Location.class));
    }

}
